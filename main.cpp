#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <emmintrin.h>
#include <time.h>
#define max(a, b) a>b?a:b
#define min(a, b) a>b?b:a

using real = float;

void print_img(FILE* file, int img_w, int img_h, int max_shade, const uint8_t* img){
  fprintf(file, "P2\n%d %d\n%d\n", img_w, img_h, max_shade);
  for(int y=0; y<img_h; y++){
    for(int x=0; x<img_w; x++){
      fprintf(file, "%u ", img[x+y*img_w]);
    }
    fprintf(file, "\n");
  }
}

void sse2_mandel(uint8_t* img, int img_w, int img_h){
  int max_shade = 255;
  int max_iters = 255;

  __m128 img_w_4 = _mm_set_ps1(img_w);
  __m128 img_h_4 = _mm_set_ps1(img_h);

  __m128 max_shade_4 = _mm_set_ps1(max_shade);
  __m128 max_iters_4 = _mm_set_ps1(max_iters);

  //real minx = -2.0;
  __m128 minx_4 = _mm_set_ps1(-2.0);
  //real maxx = 1.0;
  __m128 maxx_4 = _mm_set_ps1(1.0);
  //real miny = -1.0;
  __m128 miny_4 = _mm_set_ps1(-1.0);
  //real maxy = 1.0;
  __m128 maxy_4 = _mm_set_ps1(1.0);

  //real cr = minx;
  __m128 cr_4 = minx_4;
  //real ci = maxy;
  __m128 ci_4 = maxy_4;

  //real dx = (maxx - minx) / img_w;
  __m128 dx_4 = _mm_div_ps(_mm_sub_ps(maxx_4, minx_4), img_w_4);
  //real dy = (miny - maxy) / img_h;
  __m128 dy_4 = _mm_div_ps(_mm_sub_ps(miny_4, maxy_4), img_h_4);

  //Some constants used throughout
  __m128 const_one_4 = _mm_set_ps1(1.0);
  __m128 const_four_4 = _mm_set_ps1(4.0);

  for(int y=0; y<img_h; y++){
    //ci+=dy;
    ci_4 = _mm_add_ps(ci_4, dy_4);
    for(int x=0; x<img_w; x+=4){
      //cr+=dx;
      __m128 dxmul_4 = _mm_set_ps(x, x+1, x+2, x+3);
      cr_4 = _mm_add_ps(minx_4, _mm_mul_ps(dx_4, dxmul_4));
      
      //real zr = 0.0, zi = 0.0;
      __m128 zr_4 = _mm_set_ps1(0.0);
      __m128 zi_4 = _mm_set_ps1(0.0);

      //real zr2 = 0.0, zi2 = 0.0;
      __m128 zr2_4 = _mm_set_ps1(0.0);
      __m128 zi2_4 = _mm_set_ps1(0.0);

      //int it=0;
      __m128 it_4 = _mm_set_ps1(0.0);
      for(int n=0; n<max_iters; n++){
        //zi = 2.0 * zr * zi + ci;
        zi_4 = _mm_mul_ps(zr_4, zi_4);
        zi_4 = _mm_add_ps(zi_4, zi_4);
        zi_4 = _mm_add_ps(zi_4, ci_4);

        //zr = zr2 - zi2 + cr;
        zr_4 = _mm_sub_ps(zr2_4, zi2_4);
        zr_4 = _mm_add_ps(zr_4, cr_4);

        //zr2 = zr * zr;
        zr2_4 = _mm_mul_ps(zr_4, zr_4);

        //zi2 = zi * zi;
        zi2_4 = _mm_mul_ps(zi_4, zi_4);

        //it += (zr2 + zi2 < 4.0);
        __m128 sum_of_squares_4 = _mm_add_ps(zr2_4, zi2_4);
        __m128 ltres_4 = _mm_cmplt_ps(sum_of_squares_4, const_four_4);
        ltres_4 = _mm_and_ps(ltres_4, const_one_4); //cmplt returns max value if false
        it_4 = _mm_add_ps(it_4, ltres_4);
      }
      //img[x+y*img_w] = it * ((double)max_shade * max_iters);
      __m128 shade_4 = _mm_mul_ps(it_4, _mm_mul_ps(max_shade_4, max_iters_4));
      __m128i shade_4i = _mm_cvtps_epi32(shade_4);
      int32_t* shades = (int32_t*)&shade_4i;

      //These are pulled out in reverse because Intel architecture is little-endian
      img[x+y*img_w] = shades[3];
      img[x+1+y*img_w] = shades[2];
      img[x+2+y*img_w] = shades[1];
      img[x+3+y*img_w] = shades[0];
    }
  }

}

void single_lane_mandel(uint8_t* img, int img_w, int img_h){
  int max_shade = 255;
  int max_iters = 255;

  real minx = -2.0;
  real maxx = 1.0;
  real miny = -1.0;
  real maxy = 1.0;

  real cr = minx;
  real ci = maxy;
  real dx = (maxx - minx) / img_w;
  real dy = (miny - maxy) / img_h;

  for(int y=0; y<img_h; y++){
    ci+=dy;
    cr=minx;
    for(int x=0; x<img_w; x++){
      cr+=dx;
      real zr = 0.0, zi = 0.0;
      real zr2 = 0.0, zi2 = 0.0;
      int it=0;
      for(int n=0; n<max_iters; n++){
        zi = 2.0 * zr * zi + ci;
        zr = zr2 - zi2 + cr;
        zr2 = zr * zr;
        zi2 = zi * zi;
        it += (zr2 + zi2 < 4.0);
      }
      img[x+y*img_w] = it * ((double)max_shade * max_iters);
    }
  }
}

int main(){
  int img_w = 9600, img_h = 6400;
  int max_shade = 255;
  uint8_t* img = (uint8_t*)malloc(img_w * img_h);
  clock_t single_start = clock();
  single_lane_mandel(img, img_w, img_h);
  clock_t single_end = clock();
  FILE* file = fopen("vanilla_mandel.pgm", "w");
  print_img(file, img_w, img_h, max_shade, img);
  fclose(file);
  printf("Single lane mandelbrot finished in %f seconds\n", (single_end - single_start)/(double)CLOCKS_PER_SEC);

  clock_t simd_start = clock();
  sse2_mandel(img, img_w, img_h);
  clock_t simd_end = clock();
  file = fopen("simd_mandel.pgm", "w");
  print_img(file, img_w, img_h, max_shade, img);
  fclose(file);
  printf("SSE2 mandelbrot finished in %f seconds\n", (simd_end - simd_start)/(double)CLOCKS_PER_SEC);
  free(img);
  return 0;
} 
